node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}
node 'win.node.consul' {
  include ::profile::base_windows
  include ::profile::dns::client
}
node /web\d?.node.consul/ {
#  class { 'os_hardening': }
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::client
}
node /manager.node.consul/ {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
}
node 'haproxy.node.consul' {
  include ::role::directory_server
}

